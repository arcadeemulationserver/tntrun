local S = minetest.get_translator("tntrun")
tntrun = {}
dofile(minetest.get_modpath("tntrun") .. "/settings.lua")

local player_jump = tntrun.player_jump
local player_speed = tntrun.player_speed

arena_lib.register_minigame("tntrun", {
	name = S("Tntrun"),
	icon = "tntrun_icon.png",
	celebration_time = 3,
	load_time = 3,
	prefix = "["..S("Tntrun").."] ",
	time_mode = "incremental",
	properties = {
		tnt_area_pos_1 = {{x = 0, y = 0, z = 0}},
		tnt_area_pos_2 = {{x = 0, y = 0, z = 0}},
		eliminate_height = 0, --bellow this height you get eliminated
	},
	in_game_physics = {
		speed = player_speed,
		jump = player_jump,
	},
	disabled_damage_types = {"punch", "fall"},
	hotbar = {
		slots = 1,
		background_image = "tntrun_gui_hotbar.png"
	},
	can_drop = false,
})

minetest.register_privilege("tntrun_admin", S("Needed for Tntrun"))

dofile(minetest.get_modpath("tntrun") .. "/utils.lua")
dofile(minetest.get_modpath("tntrun") .. "/auto.lua")
dofile(minetest.get_modpath("tntrun") .. "/tnt.lua")
dofile(minetest.get_modpath("tntrun") .. "/editor.lua")



arena_lib.on_enable("tntrun", function(arena, p_name)
	-- Perform a couple of syntax checks before enabling the arena
	local check_ok, error_message = tntrun.check_arena_properties(arena)
	if check_ok then
		check_ok, error_message = tntrun.are_tnt_areas_in_arena_region(arena)
		if not check_ok then
			minetest.chat_send_player(p_name, error_message)
			return false
		else
			return true
		end
	else
		minetest.chat_send_player(p_name, error_message)
		return false
	end
end)
