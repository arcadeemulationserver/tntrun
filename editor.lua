local S = minetest.get_translator("tntrun")

-- Helper functions

-- Returns the area selector item in the player's inventory if
-- it exists. 2nd return value is inventory list index
local function get_area_selector_item(player)
	local inv = player:get_inventory()
	for i=1, inv:get_size("main") do
		local stack = inv:get_stack("main", i)
		if stack:get_name() == "tntrun:editor_tnt_area_select" then
			return stack, i
		end
	end
end

-- Returns the currently selected TNT area of player
-- by looking for the selector item.
-- Returns index 1 as a fallback.
local function get_current_tnt_area(player)
	local index = 1
	local stack = get_area_selector_item(player)
	if stack then
		local smeta = stack:get_meta()
		index = smeta:get_int("tnt_area")
		if index < 1 then
			index = 1
		end
	end
	return index
end

-- Checks the arena for the validity of the TNT areas
-- and returns true or false.
-- In case of a failure, sends error to player
local function check_arena(arena, player_name)
	if not arena then
		return false
	end
	local ok, err = tntrun.check_tnt_areas(arena)
	if not ok then
		-- In case of invalid TNT areas, print this error message
		-- that tells the user how to fix this.
		minetest.chat_send_player(player_name, S("Error! The TNT areas are broken and cannot be edited. Reason: @1", err))
		minetest.chat_send_player(player_name, S("Either fix the TNT areas in the arena properties or use \"Remove all TNT areas\"."))
	end
	return ok
end

-- Update metadata of area selector item.
local function update_area_selector_item(arena, itemstack, set_current_area)
	local imeta = itemstack:get_meta()
	local max_area = #arena.tnt_area_pos_1
	local current_area
	if set_current_area then
		current_area = set_current_area
	else
		current_area = imeta:get_int("tnt_area")
	end
	if current_area > max_area then
		current_area = max_area
	elseif current_area < 1 then
		current_area = 1
	end
	imeta:set_int("tnt_area", current_area)
	if max_area == 0 then
		imeta:set_string("count_meta", S("N/A"))
	else
		imeta:set_string("count_meta", current_area.."/"..max_area)
	end
end



-- Waypoints to show the TNT area corners

local waypoints = {} -- Key: player name; value: list of waypoint IDs

local function show_waypoints(p_name, arena)
	if waypoints[p_name] then
		remove_waypoints(p_name)
	end
	if not tntrun.check_arena_properties(arena) then
		return
	end
	local player = minetest.get_player_by_name(p_name)
	waypoints[p_name] = {}

	do
		if not player or not player:is_player() then
			return
		end
		if type(arena.tnt_area_pos_1) ~= "table" or type(arena.tnt_area_pos_2) ~= "table" then
			return
		end
		local hid
		local current_area = get_current_tnt_area(player)
		for i=1, #arena.tnt_area_pos_1 do
			local pos1 = arena.tnt_area_pos_1[i]
			local pos2 = arena.tnt_area_pos_2[i]
			if pos1 and pos2 then
				local modifier = ""
				if i ~= current_area then
					-- Semi-transparency for non-current TNT area
					modifier = "^[opacity:127"
				end
				if vector.equals(pos1, pos2) then
					hid = player:hud_add({
						hud_elem_type = "image_waypoint",
						text = "tntrun_editor_tnt_pos_both.png"..modifier,
						world_pos = pos1,
						scale = {x = 5, y = 5}
					})
					table.insert(waypoints[p_name], hid)
				else
					hid = player:hud_add({
						hud_elem_type = "image_waypoint",
						text = "tntrun_editor_tnt_pos1.png"..modifier,
						world_pos = pos1,
						scale = {x = 5, y = 5}
					})
					table.insert(waypoints[p_name], hid)
					hid = player:hud_add({
						hud_elem_type = "image_waypoint",
						text = "tntrun_editor_tnt_pos2.png"..modifier,
						world_pos = pos2,
						scale = {x = 5, y = 5}
					})
					table.insert(waypoints[p_name], hid)
				end
			end
		end
		-- Show elimination height in the center of the arena region
		-- (if set)
		if arena.pos1 and arena.pos2 and type(arena.eliminate_height) == "number" then
			local pos1, pos2 = tntrun.sort_positions(arena.pos1, arena.pos2)
			local epos = {}
			epos.y = arena.eliminate_height
			epos.x = pos1.x + (pos2.x - pos1.x) / 2
			epos.z = pos1.z + (pos2.z - pos1.z) / 2
			hid = player:hud_add({
				hud_elem_type = "image_waypoint",
				text = "tntrun_editor_eliminate_height.png",
				world_pos = epos,
				scale = {x = 5, y = 5}
			})
			table.insert(waypoints[p_name], hid)
		end
	end
end

function remove_waypoints(p_name, arena)
	local player = minetest.get_player_by_name(p_name)
	if player and waypoints[p_name] then
		for _, waypoint_ID in pairs(waypoints[p_name]) do
			player:hud_remove(waypoint_ID)
		end
	end
	waypoints[p_name] = nil
end

arena_lib.on_join_editor("tntrun", function(p_name, arena)
	show_waypoints(p_name, arena)
end)
arena_lib.on_leave_editor("tntrun", function(p_name, arena)
	remove_waypoints(p_name, arena)
end)

-- Add editor items

minetest.register_craftitem("tntrun:editor_tnt_area_add", {
	description = S("Add TNT area"),
	inventory_image = "tntrun_editor_tnt_area_add.png",
	wield_image = "tntrun_editor_tnt_area_add.png",
	groups = { not_in_creative_inventory = 1 },
	stack_max = 1,
	on_place = function() end,
	on_drop = function() end,

	on_use = function(itemstack, user, pointed_thing)
		if not user or not user:is_player() then return end
		local umeta = user:get_meta()
		local uname = user:get_player_name()
		local mod = umeta:get_string("arena_lib_editor.mod")
		local arena_name = umeta:get_string("arena_lib_editor.arena")
		local _, arena = arena_lib.get_arena_by_name(mod, arena_name)
		if not check_arena(arena, uname) then return end

		local pos = vector.round(user:get_pos())
		local a1 = table.copy(arena.tnt_area_pos_1)
		local a2 = table.copy(arena.tnt_area_pos_2)
		table.insert(a1, pos)
		table.insert(a2, pos)
		arena_lib.change_arena_property(uname, mod, arena_name, "tnt_area_pos_1", a1, true)
		arena_lib.change_arena_property(uname, mod, arena_name, "tnt_area_pos_2", a2, true)

		local selector, s_idx = get_area_selector_item(user)
		if selector then
			-- Select the area we've just created
			update_area_selector_item(arena, selector, #a1)
			local inv = user:get_inventory()
			inv:set_stack("main", s_idx, selector)
		end
		show_waypoints(uname, arena)
	end,
})

minetest.register_craftitem("tntrun:editor_tnt_area_remove", {
	description = S("Remove selected TNT area"),
	inventory_image = "tntrun_editor_tnt_area_remove.png",
	wield_image = "tntrun_editor_tnt_area_remove.png",
	groups = { not_in_creative_inventory = 1 },
	stack_max = 1,
	on_place = function() end,
	on_drop = function() end,

	on_use = function(itemstack, user, pointed_thing)
		if not user or not user:is_player() then return end
		local umeta = user:get_meta()
		local uname = user:get_player_name()
		local mod = umeta:get_string("arena_lib_editor.mod")
		local arena_name = umeta:get_string("arena_lib_editor.arena")

		local _, arena = arena_lib.get_arena_by_name(mod, arena_name)
		if not check_arena(arena, uname) then return end

		if #arena.tnt_area_pos_1 == 0 and #arena.tnt_area_pos_2 == 0 then
			minetest.chat_send_player(uname, S("There are no TNT areas."))
			return
		end
		local a1 = table.copy(arena.tnt_area_pos_1)
		local a2 = table.copy(arena.tnt_area_pos_2)
		local index = get_current_tnt_area(user)
		if not a1[index] or not a2[index] then
			minetest.chat_send_player(uname, S("Selected TNT area does not exist."))
			return
		end
		table.remove(a1, index)
		table.remove(a2, index)
		arena_lib.change_arena_property(uname, mod, arena_name, "tnt_area_pos_1", a1, true)
		arena_lib.change_arena_property(uname, mod, arena_name, "tnt_area_pos_2", a2, true)

		local selector, s_idx = get_area_selector_item(user)
		if selector then
			update_area_selector_item(arena, selector)
			local inv = user:get_inventory()
			inv:set_stack("main", s_idx, selector)
		end
		show_waypoints(uname, arena)
	end,
})

minetest.register_craftitem("tntrun:editor_tnt_area_remove_all", {
	description = S("Remove all TNT areas"),
	inventory_image = "tntrun_editor_tnt_area_remove_all.png",
	wield_image = "tntrun_editor_tnt_area_remove_all.png",
	groups = { not_in_creative_inventory = 1 },
	stack_max = 1,
	on_place = function() end,
	on_drop = function() end,

	on_use = function(itemstack, user, pointed_thing)
		if not user or not user:is_player() then return end
		local umeta = user:get_meta()
		local uname = user:get_player_name()
		local mod = umeta:get_string("arena_lib_editor.mod")
		local arena_name = umeta:get_string("arena_lib_editor.arena")
		local _, arena = arena_lib.get_arena_by_name(mod, arena_name)
		if not arena then return end

		if #arena.tnt_area_pos_1 == 0 and #arena.tnt_area_pos_2 == 0 then
			minetest.chat_send_player(uname, S("There are no TNT areas."))
			return
		end
		arena_lib.change_arena_property(user:get_player_name(), mod, arena_name, "tnt_area_pos_1", {}, true)
		arena_lib.change_arena_property(user:get_player_name(), mod, arena_name, "tnt_area_pos_2", {}, true)

		local selector, s_idx = get_area_selector_item(user)
		if selector then
			update_area_selector_item(arena, selector)
			local inv = user:get_inventory()
			inv:set_stack("main", s_idx, selector)
		end
		show_waypoints(uname, arena)
	end,
})

local function get_area_select_function(area_change)
	return function(itemstack, user, pointed_thing)
		local imeta = itemstack:get_meta()
		local umeta = user:get_meta()
		local uname = user:get_player_name()
		local mod = umeta:get_string("arena_lib_editor.mod")
		local arena_name = umeta:get_string("arena_lib_editor.arena")
		local _, arena = arena_lib.get_arena_by_name(mod, arena_name)
		if not check_arena(arena, uname) then return end

		local current_area = imeta:get_int("tnt_area")
		current_area = current_area + area_change
		if current_area < 1 then
			current_area = 1
		end
		local max_area = #arena.tnt_area_pos_1
		if current_area > max_area then
			current_area = max_area
		end
		imeta:set_int("tnt_area", current_area)
		update_area_selector_item(arena, itemstack)
		user:set_wielded_item(itemstack)

		show_waypoints(uname, arena)
	end
end

minetest.register_craftitem("tntrun:editor_tnt_area_select", {
	description = S("Select TNT area (LMB increases, RMB decreases)"),
	inventory_image = "tntrun_editor_tnt_area_select.png",
	wield_image = "tntrun_editor_tnt_area_select.png",
	groups = { not_in_creative_inventory = 1 },
	stack_max = 1,
	on_drop = function() end,

	on_use = get_area_select_function(1),
	on_secondary_use = get_area_select_function(-1),
	on_place = get_area_select_function(-1),
})

local get_set_pos_function  = function(pos1_or_pos2)
	local property_name = "tnt_area_pos_"..tostring(pos1_or_pos2)
	return function(itemstack, user, pointed_thing)
		local umeta = user:get_meta()
		local uname = user:get_player_name()
		local mod = umeta:get_string("arena_lib_editor.mod")
		local arena_name = umeta:get_string("arena_lib_editor.arena")
		local _, arena = arena_lib.get_arena_by_name(mod, arena_name)
		if not check_arena(arena, uname) then return end

		local current_area = get_current_tnt_area(user)
		if not arena[property_name][current_area] then
			if current_area == 1 then
				minetest.chat_send_player(uname, S("No TNT areas. Create a TNT area first!"))
			else
				minetest.chat_send_player(uname, S("Selected TNT area does not exist."))
			end
			return
		end
		local pos = vector.round(user:get_pos())
		local area = table.copy(arena[property_name])
		area[current_area] = pos
		arena_lib.change_arena_property(uname, mod, arena_name, property_name, area, true)
		show_waypoints(uname, arena)
	end
end

minetest.register_craftitem("tntrun:editor_tnt_area_set", {
	description = S("Set TNT area position (LMB: pos1, RMB: pos2)"),
	inventory_image = "tntrun_editor_tnt_area_set.png",
	wield_image = "tntrun_editor_tnt_area_set.png",
	groups = { not_in_creative_inventory = 1 },
	stack_max = 1,
	on_drop = function() end,

	on_use = get_set_pos_function(1),
	on_secondary_use = get_set_pos_function(2),
	on_place = get_set_pos_function(2),
})

minetest.register_craftitem("tntrun:editor_tnt_regen", {
	description = S("Regenerate TNT"),
	inventory_image = "tntrun_editor_tnt_regen.png",
	wield_image = "tntrun_editor_tnt_regen.png",
	groups = { not_in_creative_inventory = 1 },
	stack_max = 1,
	on_place = function() end,
	on_drop = function() end,

	on_use = function(itemstack, user, pointed_thing)
		if not user or not user:is_player() then return end
		local umeta = user:get_meta()
		local uname = user:get_player_name()
		local mod = umeta:get_string("arena_lib_editor.mod")
		local arena_name = umeta:get_string("arena_lib_editor.arena")
		local _, arena = arena_lib.get_arena_by_name(mod, arena_name)
		if not check_arena(arena, uname) then return end
		local region_ok, region_err = tntrun.are_tnt_areas_in_arena_region(arena)
		if not region_ok then
			minetest.chat_send_player(uname, region_err)
			return
		end

		if not tntrun.clear_tnt(arena) then
			minetest.chat_send_player(uname, S("Warning: Arena region not set, old TNT cannot be cleared."))
		end
		tntrun.place_tnt(arena)
		minetest.chat_send_player(uname, S("TNT regenerated."))
	end,
})

minetest.register_craftitem("tntrun:editor_eliminate_height", {
	description = S("Set elimination height"),
	inventory_image = "tntrun_editor_eliminate_height.png",
	wield_image = "tntrun_editor_eliminate_height.png",
	groups = { not_in_creative_inventory = 1 },
	stack_max = 1,
	on_place = function() end,
	on_drop = function() end,

	on_use = function(itemstack, user, pointed_thing)
		if not user or not user:is_player() then return end
		local uname = user:get_player_name()
		local umeta = user:get_meta()
		local mod = umeta:get_string("arena_lib_editor.mod")
		local arena_name = umeta:get_string("arena_lib_editor.arena")

		local y = user:get_pos().y
		arena_lib.change_arena_property(uname, mod, arena_name, "eliminate_height", y, true)

		local _, arena = arena_lib.get_arena_by_name(mod, arena_name)
		if not arena then return end
		show_waypoints(uname, arena)
	end,
})

-- Add editor section

arena_lib.register_editor_section("tntrun", {
	name = S("TNT areas and elimination height"),
	icon = "tntrun_icon.png",
	give_items = function(itemstack, user, arena)
		local selector = ItemStack("tntrun:editor_tnt_area_select")
		update_area_selector_item(arena, selector)

		show_waypoints(user:get_player_name(), arena)
		return {
			"tntrun:editor_tnt_area_add",
			"tntrun:editor_tnt_area_remove",
			"tntrun:editor_tnt_area_remove_all",
			selector,
			"tntrun:editor_tnt_area_set",
			"tntrun:editor_tnt_regen",
			"tntrun:editor_eliminate_height",
		}
	end,
})

